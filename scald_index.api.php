<?php

/**
 * @file
 * Hooks for scald_index module.
 */

/**
 * This hook is invoked after resolving a video
 * deep link redirect path for a given atom.
 *
 * @param  object $atom  The atom for which a path has to be determined.
 * @param  string $lang  The language.
 * @param  string $path  The default module determined path.
 */
function hook_scald_index_path_alter($atom, $lang, &$path) {
  // Change the path
}

/**
 * This hook is invoked after gathering all atoms for a given node.
 * With this hook, other modules can add unfound atoms to the list before processing.
 *
 * @param object $node  The node for which atoms are gathered.
 * @param array  $sids  The array of atoms SID.
 */
function hook_scald_index_node_atoms_alter($node, &$sids) {
  // Add some atoms
}
