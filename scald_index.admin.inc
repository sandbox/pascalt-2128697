<?php
/**
 * @file
 * Functionalities to administer the Scald Index.
 */

/**
 * Provides the form for administering scald index.
 */
function scald_index_admin_settings_form() {

  $form['scald_index_preferred_redirect'] = array(
    '#type' => 'radios',
    '#title' => t('Preferred redirect'),
    '#description' => t('Define to what page <abbr title="Video Deep Link">VDP</abbr> should redirect.'),
    '#default_value' => variable_get('scald_index_preferred_redirect', 1),
	'#options' => array(0 => t('Redirect to atom page.'), 1 => t('Redirect to node page or atom page as fallback.'))
  );

  return system_settings_form($form);
}
